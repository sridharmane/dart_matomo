import 'package:built_value/json_object.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:dart_matomo/models/action_params.dart';
import 'package:dart_matomo/models/content_params.dart';
import 'package:dart_matomo/models/entry.dart';
import 'package:dart_matomo/models/event_params.dart';
import 'package:dart_matomo/models/matomo_params.dart';
import 'package:dart_matomo/models/required_params.dart';
import 'package:dart_matomo/models/user_info_params.dart';

part 'serializers.g.dart';

@SerializersFor(const [
  JsonObject,
  ActionParams,
  RequiredParams,
  ContentParams,
  Entry,
  EventParams,
  MatomoParams,
  UserInfoParams,
])
final Serializers serializers = (_$serializers.toBuilder()
      ..addPlugin(
        StandardJsonPlugin(),
      ))
    .build();
