import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'entry.g.dart';

class EntryFields {
  static const String timestamp = 'timestamp';
  static const String queryParams = 'query_params';
}

abstract class Entry implements Built<Entry, EntryBuilder> {
  Entry._();
  factory Entry([updates(EntryBuilder b)]) = _$Entry;
  static Serializer<Entry> get serializer => _$entrySerializer;

  @BuiltValueField(wireName: '${EntryFields.timestamp}')
  int get timestamp;
  @BuiltValueField(wireName: '${EntryFields.queryParams}')
  String get queryParams;
}
