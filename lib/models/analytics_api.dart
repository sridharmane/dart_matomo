//import 'package:logging/logging.dart';
//import 'package:patient_app/models/api/api_http_response.dart';
//import 'package:patient_app/utils/http_service.dart';
//
//class AnalyticsAppUrls {
//  String domain;
//  String base;
//
//  AnalyticsAppUrls({
//    this.domain,
//    this.base,
//  });
//
//  List<String> get _all => [
//        domain,
//        base,
//      ];
//
//  bool exists(String url) => _all.indexOf(url) > -1;
//}
//
//class AnalyticsURLSDebug extends AnalyticsAppUrls {
//  AnalyticsURLSDebug({
//    domain = 'roswellpark.org',
//    base = 'https://mytracking.roswellpark.org/matomo.php',
//  }) : super(
//          domain: domain,
//          base: base,
//        );
//}
//
//class AnalyticsURLSProd extends AnalyticsAppUrls {
//  AnalyticsURLSProd({
//    domain: 'roswellpark.org',
//    base = 'https://mytracking.roswellpark.org/matomo.php',
//  }) : super(
//          domain: domain,
//          base: base,
//        );
//}
//
//class AnalyticsAPI {
//  AnalyticsAppUrls _urls;
//  int _siteId;
//  int _apiVersion;
//
//  AnalyticsAppUrls get urls => _urls;
//  int get siteId => _siteId;
//  int get apiVersion => _apiVersion;
//
//  static AnalyticsAPI _instance = AnalyticsAPI._internal();
//
//  static AnalyticsAPI get instance => _instance;
//  Logger log = Logger('AnalyticsAPI');
//
//  AnalyticsAPI._internal() {
//    // Default mode is Prod
//    bool isDebug = false;
//    // assert only runs in debug mode, Leveraging this set mode to debug if assert executes
//    assert(isDebug = true);
//    // Set URLS based on mode
//    if (isDebug) {
//      _urls = AnalyticsURLSDebug();
//      _siteId = 21;
//      _apiVersion = 1;
//    } else {
//      _urls = AnalyticsURLSProd();
//      _siteId = 18;
//      _apiVersion = 1;
//    }
//  }
//
//  Future<APIHttpResponse> send({List<String> entries}) async {
//    try {
//      if (entries.length == 1) {
//        return await HttpService.instance.get(urls.base + entries[0]);
//      }
//      return await HttpService.instance.post(urls.base,
//          body: Map.from({
//            'requests': entries,
//          }),
//          shouldjsonEncode: true);
//    } catch (e, stackTrace) {
//      log.severe('Error Uploading Analytics', e, stackTrace);
//      return null;
//    }
//  }
//}
