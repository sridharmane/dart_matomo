// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'content_params.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ContentParams> _$contentParamsSerializer =
    new _$ContentParamsSerializer();

class _$ContentParamsSerializer implements StructuredSerializer<ContentParams> {
  @override
  final Iterable<Type> types = const [ContentParams, _$ContentParams];
  @override
  final String wireName = 'ContentParams';

  @override
  Iterable<Object> serialize(Serializers serializers, ContentParams object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.name != null) {
      result
        ..add('c_n')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.contentPiece != null) {
      result
        ..add('c_p')
        ..add(serializers.serialize(object.contentPiece,
            specifiedType: const FullType(String)));
    }
    if (object.target != null) {
      result
        ..add('c_t')
        ..add(serializers.serialize(object.target,
            specifiedType: const FullType(String)));
    }
    if (object.interactionName != null) {
      result
        ..add('c_i')
        ..add(serializers.serialize(object.interactionName,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  ContentParams deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ContentParamsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'c_n':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'c_p':
          result.contentPiece = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'c_t':
          result.target = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'c_i':
          result.interactionName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ContentParams extends ContentParams {
  @override
  final String name;
  @override
  final String contentPiece;
  @override
  final String target;
  @override
  final String interactionName;

  factory _$ContentParams([void Function(ContentParamsBuilder) updates]) =>
      (new ContentParamsBuilder()..update(updates)).build();

  _$ContentParams._(
      {this.name, this.contentPiece, this.target, this.interactionName})
      : super._();

  @override
  ContentParams rebuild(void Function(ContentParamsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ContentParamsBuilder toBuilder() => new ContentParamsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ContentParams &&
        name == other.name &&
        contentPiece == other.contentPiece &&
        target == other.target &&
        interactionName == other.interactionName;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, name.hashCode), contentPiece.hashCode), target.hashCode),
        interactionName.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ContentParams')
          ..add('name', name)
          ..add('contentPiece', contentPiece)
          ..add('target', target)
          ..add('interactionName', interactionName))
        .toString();
  }
}

class ContentParamsBuilder
    implements Builder<ContentParams, ContentParamsBuilder> {
  _$ContentParams _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _contentPiece;
  String get contentPiece => _$this._contentPiece;
  set contentPiece(String contentPiece) => _$this._contentPiece = contentPiece;

  String _target;
  String get target => _$this._target;
  set target(String target) => _$this._target = target;

  String _interactionName;
  String get interactionName => _$this._interactionName;
  set interactionName(String interactionName) =>
      _$this._interactionName = interactionName;

  ContentParamsBuilder();

  ContentParamsBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _contentPiece = _$v.contentPiece;
      _target = _$v.target;
      _interactionName = _$v.interactionName;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ContentParams other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ContentParams;
  }

  @override
  void update(void Function(ContentParamsBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ContentParams build() {
    final _$result = _$v ??
        new _$ContentParams._(
            name: name,
            contentPiece: contentPiece,
            target: target,
            interactionName: interactionName);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
