import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'user_info_params.g.dart';

abstract class UserInfoParams
    implements Built<UserInfoParams, UserInfoParamsBuilder> {
  UserInfoParams._();
  factory UserInfoParams([updates(UserInfoParamsBuilder b)]) = _$UserInfoParams;
  static Serializer<UserInfoParams> get serializer =>
      _$userInfoParamsSerializer;

  @nullable
  @BuiltValueField(wireName: 'uid')
  String get uid;

  @nullable
  @BuiltValueField(wireName: '_idvc')
  int get visitCount;

  @nullable
  @BuiltValueField(wireName: '_viewts')
  int get lastVisitDateTime;

  @nullable
  @BuiltValueField(wireName: 'res')
  String get screenResolution;

  @nullable
  @BuiltValueField(wireName: 'h')
  String get hour;

  @nullable
  @BuiltValueField(wireName: 'm')
  String get min;

  @nullable
  @BuiltValueField(wireName: 's')
  String get sec;
}
