import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'event_params.g.dart';

abstract class EventParams implements Built<EventParams, EventParamsBuilder> {
  EventParams._();
  factory EventParams([updates(EventParamsBuilder b)]) = _$EventParams;
  static Serializer<EventParams> get serializer => _$eventParamsSerializer;

  @nullable
  @BuiltValueField(wireName: 'e_c')
  String get category;
  @nullable
  @BuiltValueField(wireName: 'e_a')
  String get action;
  @nullable
  @BuiltValueField(wireName: 'e_n')
  String get name;
  @nullable
  @BuiltValueField(wireName: 'e_v')
  double get value;
}
