// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_params.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<EventParams> _$eventParamsSerializer = new _$EventParamsSerializer();

class _$EventParamsSerializer implements StructuredSerializer<EventParams> {
  @override
  final Iterable<Type> types = const [EventParams, _$EventParams];
  @override
  final String wireName = 'EventParams';

  @override
  Iterable<Object> serialize(Serializers serializers, EventParams object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.category != null) {
      result
        ..add('e_c')
        ..add(serializers.serialize(object.category,
            specifiedType: const FullType(String)));
    }
    if (object.action != null) {
      result
        ..add('e_a')
        ..add(serializers.serialize(object.action,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('e_n')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.value != null) {
      result
        ..add('e_v')
        ..add(serializers.serialize(object.value,
            specifiedType: const FullType(double)));
    }
    return result;
  }

  @override
  EventParams deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new EventParamsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'e_c':
          result.category = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'e_a':
          result.action = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'e_n':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'e_v':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
      }
    }

    return result.build();
  }
}

class _$EventParams extends EventParams {
  @override
  final String category;
  @override
  final String action;
  @override
  final String name;
  @override
  final double value;

  factory _$EventParams([void Function(EventParamsBuilder) updates]) =>
      (new EventParamsBuilder()..update(updates)).build();

  _$EventParams._({this.category, this.action, this.name, this.value})
      : super._();

  @override
  EventParams rebuild(void Function(EventParamsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  EventParamsBuilder toBuilder() => new EventParamsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EventParams &&
        category == other.category &&
        action == other.action &&
        name == other.name &&
        value == other.value;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, category.hashCode), action.hashCode), name.hashCode),
        value.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('EventParams')
          ..add('category', category)
          ..add('action', action)
          ..add('name', name)
          ..add('value', value))
        .toString();
  }
}

class EventParamsBuilder implements Builder<EventParams, EventParamsBuilder> {
  _$EventParams _$v;

  String _category;
  String get category => _$this._category;
  set category(String category) => _$this._category = category;

  String _action;
  String get action => _$this._action;
  set action(String action) => _$this._action = action;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  double _value;
  double get value => _$this._value;
  set value(double value) => _$this._value = value;

  EventParamsBuilder();

  EventParamsBuilder get _$this {
    if (_$v != null) {
      _category = _$v.category;
      _action = _$v.action;
      _name = _$v.name;
      _value = _$v.value;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EventParams other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$EventParams;
  }

  @override
  void update(void Function(EventParamsBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$EventParams build() {
    final _$result = _$v ??
        new _$EventParams._(
            category: category, action: action, name: name, value: value);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
