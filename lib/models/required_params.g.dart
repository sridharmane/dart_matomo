// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'required_params.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<RequiredParams> _$requiredParamsSerializer =
    new _$RequiredParamsSerializer();

class _$RequiredParamsSerializer
    implements StructuredSerializer<RequiredParams> {
  @override
  final Iterable<Type> types = const [RequiredParams, _$RequiredParams];
  @override
  final String wireName = 'RequiredParams';

  @override
  Iterable<Object> serialize(Serializers serializers, RequiredParams object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'idsite',
      serializers.serialize(object.siteId, specifiedType: const FullType(int)),
      'rec',
      serializers.serialize(object.record, specifiedType: const FullType(int)),
      'apiv',
      serializers.serialize(object.apiVersion,
          specifiedType: const FullType(int)),
      'ua',
      serializers.serialize(object.userAgent,
          specifiedType: const FullType(String)),
    ];
    if (object.dateTime != null) {
      result
        ..add('cdt')
        ..add(serializers.serialize(object.dateTime,
            specifiedType: const FullType(int)));
    }
    if (object.actionName != null) {
      result
        ..add('action_name')
        ..add(serializers.serialize(object.actionName,
            specifiedType: const FullType(String)));
    }
    if (object.url != null) {
      result
        ..add('url')
        ..add(serializers.serialize(object.url,
            specifiedType: const FullType(String)));
    }
    if (object.deviceId != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.deviceId,
            specifiedType: const FullType(String)));
    }
    if (object.randStr != null) {
      result
        ..add('rand')
        ..add(serializers.serialize(object.randStr,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  RequiredParams deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new RequiredParamsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'idsite':
          result.siteId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'rec':
          result.record = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'apiv':
          result.apiVersion = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'ua':
          result.userAgent = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'cdt':
          result.dateTime = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'action_name':
          result.actionName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'url':
          result.url = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_id':
          result.deviceId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'rand':
          result.randStr = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$RequiredParams extends RequiredParams {
  @override
  final int siteId;
  @override
  final int record;
  @override
  final int apiVersion;
  @override
  final String userAgent;
  @override
  final int dateTime;
  @override
  final String actionName;
  @override
  final String url;
  @override
  final String deviceId;
  @override
  final String randStr;

  factory _$RequiredParams([void Function(RequiredParamsBuilder) updates]) =>
      (new RequiredParamsBuilder()..update(updates)).build();

  _$RequiredParams._(
      {this.siteId,
      this.record,
      this.apiVersion,
      this.userAgent,
      this.dateTime,
      this.actionName,
      this.url,
      this.deviceId,
      this.randStr})
      : super._() {
    if (siteId == null) {
      throw new BuiltValueNullFieldError('RequiredParams', 'siteId');
    }
    if (record == null) {
      throw new BuiltValueNullFieldError('RequiredParams', 'record');
    }
    if (apiVersion == null) {
      throw new BuiltValueNullFieldError('RequiredParams', 'apiVersion');
    }
    if (userAgent == null) {
      throw new BuiltValueNullFieldError('RequiredParams', 'userAgent');
    }
  }

  @override
  RequiredParams rebuild(void Function(RequiredParamsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RequiredParamsBuilder toBuilder() =>
      new RequiredParamsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RequiredParams &&
        siteId == other.siteId &&
        record == other.record &&
        apiVersion == other.apiVersion &&
        userAgent == other.userAgent &&
        dateTime == other.dateTime &&
        actionName == other.actionName &&
        url == other.url &&
        deviceId == other.deviceId &&
        randStr == other.randStr;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc($jc($jc(0, siteId.hashCode), record.hashCode),
                                apiVersion.hashCode),
                            userAgent.hashCode),
                        dateTime.hashCode),
                    actionName.hashCode),
                url.hashCode),
            deviceId.hashCode),
        randStr.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RequiredParams')
          ..add('siteId', siteId)
          ..add('record', record)
          ..add('apiVersion', apiVersion)
          ..add('userAgent', userAgent)
          ..add('dateTime', dateTime)
          ..add('actionName', actionName)
          ..add('url', url)
          ..add('deviceId', deviceId)
          ..add('randStr', randStr))
        .toString();
  }
}

class RequiredParamsBuilder
    implements Builder<RequiredParams, RequiredParamsBuilder> {
  _$RequiredParams _$v;

  int _siteId;
  int get siteId => _$this._siteId;
  set siteId(int siteId) => _$this._siteId = siteId;

  int _record;
  int get record => _$this._record;
  set record(int record) => _$this._record = record;

  int _apiVersion;
  int get apiVersion => _$this._apiVersion;
  set apiVersion(int apiVersion) => _$this._apiVersion = apiVersion;

  String _userAgent;
  String get userAgent => _$this._userAgent;
  set userAgent(String userAgent) => _$this._userAgent = userAgent;

  int _dateTime;
  int get dateTime => _$this._dateTime;
  set dateTime(int dateTime) => _$this._dateTime = dateTime;

  String _actionName;
  String get actionName => _$this._actionName;
  set actionName(String actionName) => _$this._actionName = actionName;

  String _url;
  String get url => _$this._url;
  set url(String url) => _$this._url = url;

  String _deviceId;
  String get deviceId => _$this._deviceId;
  set deviceId(String deviceId) => _$this._deviceId = deviceId;

  String _randStr;
  String get randStr => _$this._randStr;
  set randStr(String randStr) => _$this._randStr = randStr;

  RequiredParamsBuilder();

  RequiredParamsBuilder get _$this {
    if (_$v != null) {
      _siteId = _$v.siteId;
      _record = _$v.record;
      _apiVersion = _$v.apiVersion;
      _userAgent = _$v.userAgent;
      _dateTime = _$v.dateTime;
      _actionName = _$v.actionName;
      _url = _$v.url;
      _deviceId = _$v.deviceId;
      _randStr = _$v.randStr;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RequiredParams other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$RequiredParams;
  }

  @override
  void update(void Function(RequiredParamsBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RequiredParams build() {
    final _$result = _$v ??
        new _$RequiredParams._(
            siteId: siteId,
            record: record,
            apiVersion: apiVersion,
            userAgent: userAgent,
            dateTime: dateTime,
            actionName: actionName,
            url: url,
            deviceId: deviceId,
            randStr: randStr);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
