// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'db.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class MatomoEntryData extends DataClass implements Insertable<MatomoEntryData> {
  final DateTime timestamp;
  final String queryParams;
  MatomoEntryData({@required this.timestamp, @required this.queryParams});
  factory MatomoEntryData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    final stringType = db.typeSystem.forDartType<String>();
    return MatomoEntryData(
      timestamp: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}timestamp']),
      queryParams: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}query_params']),
    );
  }
  factory MatomoEntryData.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return MatomoEntryData(
      timestamp: serializer.fromJson<DateTime>(json['timestamp']),
      queryParams: serializer.fromJson<String>(json['queryParams']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'timestamp': serializer.toJson<DateTime>(timestamp),
      'queryParams': serializer.toJson<String>(queryParams),
    };
  }

  @override
  MatomoEntryCompanion createCompanion(bool nullToAbsent) {
    return MatomoEntryCompanion(
      timestamp: timestamp == null && nullToAbsent
          ? const Value.absent()
          : Value(timestamp),
      queryParams: queryParams == null && nullToAbsent
          ? const Value.absent()
          : Value(queryParams),
    );
  }

  MatomoEntryData copyWith({DateTime timestamp, String queryParams}) =>
      MatomoEntryData(
        timestamp: timestamp ?? this.timestamp,
        queryParams: queryParams ?? this.queryParams,
      );
  @override
  String toString() {
    return (StringBuffer('MatomoEntryData(')
          ..write('timestamp: $timestamp, ')
          ..write('queryParams: $queryParams')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(timestamp.hashCode, queryParams.hashCode));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is MatomoEntryData &&
          other.timestamp == this.timestamp &&
          other.queryParams == this.queryParams);
}

class MatomoEntryCompanion extends UpdateCompanion<MatomoEntryData> {
  final Value<DateTime> timestamp;
  final Value<String> queryParams;
  const MatomoEntryCompanion({
    this.timestamp = const Value.absent(),
    this.queryParams = const Value.absent(),
  });
  MatomoEntryCompanion.insert({
    @required DateTime timestamp,
    @required String queryParams,
  })  : timestamp = Value(timestamp),
        queryParams = Value(queryParams);
  MatomoEntryCompanion copyWith(
      {Value<DateTime> timestamp, Value<String> queryParams}) {
    return MatomoEntryCompanion(
      timestamp: timestamp ?? this.timestamp,
      queryParams: queryParams ?? this.queryParams,
    );
  }
}

class $MatomoEntryTable extends MatomoEntry
    with TableInfo<$MatomoEntryTable, MatomoEntryData> {
  final GeneratedDatabase _db;
  final String _alias;
  $MatomoEntryTable(this._db, [this._alias]);
  final VerificationMeta _timestampMeta = const VerificationMeta('timestamp');
  GeneratedDateTimeColumn _timestamp;
  @override
  GeneratedDateTimeColumn get timestamp => _timestamp ??= _constructTimestamp();
  GeneratedDateTimeColumn _constructTimestamp() {
    return GeneratedDateTimeColumn(
      'timestamp',
      $tableName,
      false,
    );
  }

  final VerificationMeta _queryParamsMeta =
      const VerificationMeta('queryParams');
  GeneratedTextColumn _queryParams;
  @override
  GeneratedTextColumn get queryParams =>
      _queryParams ??= _constructQueryParams();
  GeneratedTextColumn _constructQueryParams() {
    return GeneratedTextColumn(
      'query_params',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [timestamp, queryParams];
  @override
  $MatomoEntryTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'matomo_entry';
  @override
  final String actualTableName = 'matomo_entry';
  @override
  VerificationContext validateIntegrity(MatomoEntryCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.timestamp.present) {
      context.handle(_timestampMeta,
          timestamp.isAcceptableValue(d.timestamp.value, _timestampMeta));
    } else if (isInserting) {
      context.missing(_timestampMeta);
    }
    if (d.queryParams.present) {
      context.handle(_queryParamsMeta,
          queryParams.isAcceptableValue(d.queryParams.value, _queryParamsMeta));
    } else if (isInserting) {
      context.missing(_queryParamsMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  MatomoEntryData map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return MatomoEntryData.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(MatomoEntryCompanion d) {
    final map = <String, Variable>{};
    if (d.timestamp.present) {
      map['timestamp'] = Variable<DateTime, DateTimeType>(d.timestamp.value);
    }
    if (d.queryParams.present) {
      map['query_params'] = Variable<String, StringType>(d.queryParams.value);
    }
    return map;
  }

  @override
  $MatomoEntryTable createAlias(String alias) {
    return $MatomoEntryTable(_db, alias);
  }
}

abstract class _$MatomoDatabase extends GeneratedDatabase {
  _$MatomoDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $MatomoEntryTable _matomoEntry;
  $MatomoEntryTable get matomoEntry => _matomoEntry ??= $MatomoEntryTable(this);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [matomoEntry];
}
