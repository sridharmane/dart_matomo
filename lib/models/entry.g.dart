// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'entry.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Entry> _$entrySerializer = new _$EntrySerializer();

class _$EntrySerializer implements StructuredSerializer<Entry> {
  @override
  final Iterable<Type> types = const [Entry, _$Entry];
  @override
  final String wireName = 'Entry';

  @override
  Iterable<Object> serialize(Serializers serializers, Entry object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'timestamp',
      serializers.serialize(object.timestamp,
          specifiedType: const FullType(int)),
      'query_params',
      serializers.serialize(object.queryParams,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  Entry deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new EntryBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'query_params':
          result.queryParams = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Entry extends Entry {
  @override
  final int timestamp;
  @override
  final String queryParams;

  factory _$Entry([void Function(EntryBuilder) updates]) =>
      (new EntryBuilder()..update(updates)).build();

  _$Entry._({this.timestamp, this.queryParams}) : super._() {
    if (timestamp == null) {
      throw new BuiltValueNullFieldError('Entry', 'timestamp');
    }
    if (queryParams == null) {
      throw new BuiltValueNullFieldError('Entry', 'queryParams');
    }
  }

  @override
  Entry rebuild(void Function(EntryBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  EntryBuilder toBuilder() => new EntryBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Entry &&
        timestamp == other.timestamp &&
        queryParams == other.queryParams;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, timestamp.hashCode), queryParams.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Entry')
          ..add('timestamp', timestamp)
          ..add('queryParams', queryParams))
        .toString();
  }
}

class EntryBuilder implements Builder<Entry, EntryBuilder> {
  _$Entry _$v;

  int _timestamp;
  int get timestamp => _$this._timestamp;
  set timestamp(int timestamp) => _$this._timestamp = timestamp;

  String _queryParams;
  String get queryParams => _$this._queryParams;
  set queryParams(String queryParams) => _$this._queryParams = queryParams;

  EntryBuilder();

  EntryBuilder get _$this {
    if (_$v != null) {
      _timestamp = _$v.timestamp;
      _queryParams = _$v.queryParams;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Entry other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Entry;
  }

  @override
  void update(void Function(EntryBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Entry build() {
    final _$result =
        _$v ?? new _$Entry._(timestamp: timestamp, queryParams: queryParams);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
