import 'package:built_value/built_value.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/serializer.dart';

part 'action_params.g.dart';

abstract class ActionParams
    implements Built<ActionParams, ActionParamsBuilder> {
  ActionParams._();
  factory ActionParams([updates(ActionParamsBuilder b)]) = _$ActionParams;
  static Serializer<ActionParams> get serializer => _$actionParamsSerializer;

  /// Page scope custom variables. This is a JSON encoded string of the custom
  /// variable array (see below for an example value).
  @nullable
  @BuiltValueField(wireName: 'cvar')
  JsonObject get customVariables;
  @nullable
  @BuiltValueField(wireName: 'link')
  String get externalUrl;
  @nullable
  @BuiltValueField(wireName: 'download')
  String get downloadUrl;
  @nullable
  @BuiltValueField(wireName: 'search')
  String get search;
  @nullable
  @BuiltValueField(wireName: 'search_cat')
  String get searchCategory;
  @nullable
  @BuiltValueField(wireName: 'search_count')
  int get searchCount;

  /// Accepts a six character unique ID that identifies which actions were
  /// performed on a specific page view. When a page was viewed, all following
  /// tracking requests (such as events) during that page view should use the
  /// same pageview ID. Once another page was viewed a new unique ID should be
  /// generated. Use [0-9a-Z] as possible characters for the unique ID.
  @nullable
  @BuiltValueField(wireName: 'pv_id')
  String get pageViewId;
  @nullable
  @BuiltValueField(wireName: 'idglobal')
  String get goalId;

  ///
  /// The amount of time it took the server to generate this action, in milliseconds.
  /// This value is used to process the Page speed report Avg. generation time
  /// column in the Page URL and Page Title reports, as well as a site wide
  /// running average of the speed of your server.
  ///
  @nullable
  @BuiltValueField(wireName: 'gt_ms')
  int get generationTime;
}
