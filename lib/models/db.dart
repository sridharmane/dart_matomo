import 'dart:io';

import 'package:dart_matomo/models/entry.dart';
import 'package:dart_matomo/models/matomo_entry.dart';
import 'package:moor/moor.dart';
import 'package:moor_ffi/moor_ffi.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';

part 'db.g.dart';

LazyDatabase _openConnection() {
  // the LazyDatabase util lets us find the right location for the file async.
  return LazyDatabase(() async {
    // put the database file, called db.sqlite here, into the documents folder
    // for your app.
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(path.join(dbFolder.path, 'matomo_db.sqlite'));
    return VmDatabase(file);
  });
}

@UseMoor(tables: [MatomoEntry])
class MatomoDatabase extends _$MatomoDatabase {
  // we tell the database where to store the data with this constructor
  MatomoDatabase() : super(_openConnection());

  // you should bump this number whenever you change or add a table definition. Migrations
  // are covered later in this readme.
  @override
  int get schemaVersion => 1;

  void addEntry(Entry entry) {}
}
