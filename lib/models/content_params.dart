import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'content_params.g.dart';

abstract class ContentParams
    implements Built<ContentParams, ContentParamsBuilder> {
  ContentParams._();
  factory ContentParams([updates(ContentParamsBuilder b)]) = _$ContentParams;
  static Serializer<ContentParams> get serializer => _$contentParamsSerializer;

  /// The name of the content. For instance 'Ad Foo Bar'
  @nullable
  @BuiltValueField(wireName: 'c_n')
  String get name;

  /// The actual content piece. For instance the path to an image, video, audio, any text
  @nullable
  @BuiltValueField(wireName: 'c_p')
  String get contentPiece;

  /// The target of the content. For instance the URL of a landing page
  @nullable
  @BuiltValueField(wireName: 'c_t')
  String get target;

  /// The name of the interaction with the content. For instance a 'click'
  @nullable
  @BuiltValueField(wireName: 'c_i')
  String get interactionName;
}
