import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'required_params.g.dart';

abstract class RequiredParams
    implements Built<RequiredParams, RequiredParamsBuilder> {
  RequiredParams._();
  factory RequiredParams([updates(RequiredParamsBuilder b)]) = _$RequiredParams;
  static Serializer<RequiredParams> get serializer =>
      _$requiredParamsSerializer;

  @BuiltValueField(wireName: 'idsite')
  int get siteId;
  @BuiltValueField(wireName: 'rec')
  int get record;

  @BuiltValueField(wireName: 'apiv')
  int get apiVersion;

  @BuiltValueField(wireName: 'ua')
  String get userAgent;

  ///
  /// cdt: used to set the timestamp of when the event occured.
  /// Events older than 24 hrs should be discarded as then need a token_id
  ///
  @nullable
  @BuiltValueField(wireName: 'cdt')
  int get dateTime;
  @nullable
  @BuiltValueField(wireName: 'action_name')
  String get actionName;
  @nullable
  @BuiltValueField(wireName: 'url')
  String get url;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get deviceId;
  @nullable
  @BuiltValueField(wireName: 'rand')
  String get randStr;
}
