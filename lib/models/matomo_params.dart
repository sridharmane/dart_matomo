import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:dart_matomo/models/action_params.dart';
import 'package:dart_matomo/models/content_params.dart';
import 'package:dart_matomo/models/event_params.dart';
import 'package:dart_matomo/models/required_params.dart';
import 'package:dart_matomo/models/serializers.dart';
import 'package:dart_matomo/models/user_info_params.dart';

part 'matomo_params.g.dart';

abstract class MatomoParams
    implements Built<MatomoParams, MatomoParamsBuilder> {
  MatomoParams._();
  factory MatomoParams([updates(MatomoParamsBuilder b)]) = _$MatomoParams;
  static Serializer<MatomoParams> get serializer => _$matomoParamsSerializer;

  RequiredParams get requiredParams;

  @nullable
  UserInfoParams get userParams;

  @nullable
  EventParams get eventParams;

  @nullable
  ContentParams get contentParams;

  @nullable
  ActionParams get actionParams;

  /// Converts all the params in the class into query format like format
  /// [AnalyticsParam.key]=[AnalyticsParam.value]&
  String get asQueryParams {
    Map<String, dynamic> allParams = Map();
    allParams.addEntries(
      (serializers.serializeWith(
        RequiredParams.serializer,
        requiredParams,
      ) as Map)
          .entries,
    );
    if (userParams != null) {
      allParams.addEntries(
        (serializers.serializeWith(
          UserInfoParams.serializer,
          userParams,
        ) as Map)
            .entries,
      );
    }
    if (contentParams != null) {
      allParams.addEntries(
        (serializers.serializeWith(
          ContentParams.serializer,
          contentParams,
        ) as Map)
            .entries,
      );
    }
    if (actionParams != null) {
      allParams.addEntries(
        (serializers.serializeWith(
          ActionParams.serializer,
          actionParams,
        ) as Map)
            .entries,
      );
    }
    allParams.removeWhere((key, val) => val == null);
    return '?' +
        allParams.keys.map((key) {
          if (allParams[key] is String) {
            return '$key=${Uri.encodeComponent(allParams[key])}';
          }
          return '$key=${allParams[key]}';
        }).join('&');
  }
}
