// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'matomo_params.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<MatomoParams> _$matomoParamsSerializer =
    new _$MatomoParamsSerializer();

class _$MatomoParamsSerializer implements StructuredSerializer<MatomoParams> {
  @override
  final Iterable<Type> types = const [MatomoParams, _$MatomoParams];
  @override
  final String wireName = 'MatomoParams';

  @override
  Iterable<Object> serialize(Serializers serializers, MatomoParams object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'requiredParams',
      serializers.serialize(object.requiredParams,
          specifiedType: const FullType(RequiredParams)),
    ];
    if (object.userParams != null) {
      result
        ..add('userParams')
        ..add(serializers.serialize(object.userParams,
            specifiedType: const FullType(UserInfoParams)));
    }
    if (object.eventParams != null) {
      result
        ..add('eventParams')
        ..add(serializers.serialize(object.eventParams,
            specifiedType: const FullType(EventParams)));
    }
    if (object.contentParams != null) {
      result
        ..add('contentParams')
        ..add(serializers.serialize(object.contentParams,
            specifiedType: const FullType(ContentParams)));
    }
    if (object.actionParams != null) {
      result
        ..add('actionParams')
        ..add(serializers.serialize(object.actionParams,
            specifiedType: const FullType(ActionParams)));
    }
    return result;
  }

  @override
  MatomoParams deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new MatomoParamsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'requiredParams':
          result.requiredParams.replace(serializers.deserialize(value,
              specifiedType: const FullType(RequiredParams)) as RequiredParams);
          break;
        case 'userParams':
          result.userParams.replace(serializers.deserialize(value,
              specifiedType: const FullType(UserInfoParams)) as UserInfoParams);
          break;
        case 'eventParams':
          result.eventParams.replace(serializers.deserialize(value,
              specifiedType: const FullType(EventParams)) as EventParams);
          break;
        case 'contentParams':
          result.contentParams.replace(serializers.deserialize(value,
              specifiedType: const FullType(ContentParams)) as ContentParams);
          break;
        case 'actionParams':
          result.actionParams.replace(serializers.deserialize(value,
              specifiedType: const FullType(ActionParams)) as ActionParams);
          break;
      }
    }

    return result.build();
  }
}

class _$MatomoParams extends MatomoParams {
  @override
  final RequiredParams requiredParams;
  @override
  final UserInfoParams userParams;
  @override
  final EventParams eventParams;
  @override
  final ContentParams contentParams;
  @override
  final ActionParams actionParams;

  factory _$MatomoParams([void Function(MatomoParamsBuilder) updates]) =>
      (new MatomoParamsBuilder()..update(updates)).build();

  _$MatomoParams._(
      {this.requiredParams,
      this.userParams,
      this.eventParams,
      this.contentParams,
      this.actionParams})
      : super._() {
    if (requiredParams == null) {
      throw new BuiltValueNullFieldError('MatomoParams', 'requiredParams');
    }
  }

  @override
  MatomoParams rebuild(void Function(MatomoParamsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MatomoParamsBuilder toBuilder() => new MatomoParamsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MatomoParams &&
        requiredParams == other.requiredParams &&
        userParams == other.userParams &&
        eventParams == other.eventParams &&
        contentParams == other.contentParams &&
        actionParams == other.actionParams;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, requiredParams.hashCode), userParams.hashCode),
                eventParams.hashCode),
            contentParams.hashCode),
        actionParams.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MatomoParams')
          ..add('requiredParams', requiredParams)
          ..add('userParams', userParams)
          ..add('eventParams', eventParams)
          ..add('contentParams', contentParams)
          ..add('actionParams', actionParams))
        .toString();
  }
}

class MatomoParamsBuilder
    implements Builder<MatomoParams, MatomoParamsBuilder> {
  _$MatomoParams _$v;

  RequiredParamsBuilder _requiredParams;
  RequiredParamsBuilder get requiredParams =>
      _$this._requiredParams ??= new RequiredParamsBuilder();
  set requiredParams(RequiredParamsBuilder requiredParams) =>
      _$this._requiredParams = requiredParams;

  UserInfoParamsBuilder _userParams;
  UserInfoParamsBuilder get userParams =>
      _$this._userParams ??= new UserInfoParamsBuilder();
  set userParams(UserInfoParamsBuilder userParams) =>
      _$this._userParams = userParams;

  EventParamsBuilder _eventParams;
  EventParamsBuilder get eventParams =>
      _$this._eventParams ??= new EventParamsBuilder();
  set eventParams(EventParamsBuilder eventParams) =>
      _$this._eventParams = eventParams;

  ContentParamsBuilder _contentParams;
  ContentParamsBuilder get contentParams =>
      _$this._contentParams ??= new ContentParamsBuilder();
  set contentParams(ContentParamsBuilder contentParams) =>
      _$this._contentParams = contentParams;

  ActionParamsBuilder _actionParams;
  ActionParamsBuilder get actionParams =>
      _$this._actionParams ??= new ActionParamsBuilder();
  set actionParams(ActionParamsBuilder actionParams) =>
      _$this._actionParams = actionParams;

  MatomoParamsBuilder();

  MatomoParamsBuilder get _$this {
    if (_$v != null) {
      _requiredParams = _$v.requiredParams?.toBuilder();
      _userParams = _$v.userParams?.toBuilder();
      _eventParams = _$v.eventParams?.toBuilder();
      _contentParams = _$v.contentParams?.toBuilder();
      _actionParams = _$v.actionParams?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MatomoParams other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$MatomoParams;
  }

  @override
  void update(void Function(MatomoParamsBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$MatomoParams build() {
    _$MatomoParams _$result;
    try {
      _$result = _$v ??
          new _$MatomoParams._(
              requiredParams: requiredParams.build(),
              userParams: _userParams?.build(),
              eventParams: _eventParams?.build(),
              contentParams: _contentParams?.build(),
              actionParams: _actionParams?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'requiredParams';
        requiredParams.build();
        _$failedField = 'userParams';
        _userParams?.build();
        _$failedField = 'eventParams';
        _eventParams?.build();
        _$failedField = 'contentParams';
        _contentParams?.build();
        _$failedField = 'actionParams';
        _actionParams?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'MatomoParams', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
