// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_info_params.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UserInfoParams> _$userInfoParamsSerializer =
    new _$UserInfoParamsSerializer();

class _$UserInfoParamsSerializer
    implements StructuredSerializer<UserInfoParams> {
  @override
  final Iterable<Type> types = const [UserInfoParams, _$UserInfoParams];
  @override
  final String wireName = 'UserInfoParams';

  @override
  Iterable<Object> serialize(Serializers serializers, UserInfoParams object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.uid != null) {
      result
        ..add('uid')
        ..add(serializers.serialize(object.uid,
            specifiedType: const FullType(String)));
    }
    if (object.visitCount != null) {
      result
        ..add('_idvc')
        ..add(serializers.serialize(object.visitCount,
            specifiedType: const FullType(int)));
    }
    if (object.lastVisitDateTime != null) {
      result
        ..add('_viewts')
        ..add(serializers.serialize(object.lastVisitDateTime,
            specifiedType: const FullType(int)));
    }
    if (object.screenResolution != null) {
      result
        ..add('res')
        ..add(serializers.serialize(object.screenResolution,
            specifiedType: const FullType(String)));
    }
    if (object.hour != null) {
      result
        ..add('h')
        ..add(serializers.serialize(object.hour,
            specifiedType: const FullType(String)));
    }
    if (object.min != null) {
      result
        ..add('m')
        ..add(serializers.serialize(object.min,
            specifiedType: const FullType(String)));
    }
    if (object.sec != null) {
      result
        ..add('s')
        ..add(serializers.serialize(object.sec,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  UserInfoParams deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserInfoParamsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'uid':
          result.uid = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_idvc':
          result.visitCount = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case '_viewts':
          result.lastVisitDateTime = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'res':
          result.screenResolution = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'h':
          result.hour = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'm':
          result.min = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 's':
          result.sec = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$UserInfoParams extends UserInfoParams {
  @override
  final String uid;
  @override
  final int visitCount;
  @override
  final int lastVisitDateTime;
  @override
  final String screenResolution;
  @override
  final String hour;
  @override
  final String min;
  @override
  final String sec;

  factory _$UserInfoParams([void Function(UserInfoParamsBuilder) updates]) =>
      (new UserInfoParamsBuilder()..update(updates)).build();

  _$UserInfoParams._(
      {this.uid,
      this.visitCount,
      this.lastVisitDateTime,
      this.screenResolution,
      this.hour,
      this.min,
      this.sec})
      : super._();

  @override
  UserInfoParams rebuild(void Function(UserInfoParamsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserInfoParamsBuilder toBuilder() =>
      new UserInfoParamsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserInfoParams &&
        uid == other.uid &&
        visitCount == other.visitCount &&
        lastVisitDateTime == other.lastVisitDateTime &&
        screenResolution == other.screenResolution &&
        hour == other.hour &&
        min == other.min &&
        sec == other.sec;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, uid.hashCode), visitCount.hashCode),
                        lastVisitDateTime.hashCode),
                    screenResolution.hashCode),
                hour.hashCode),
            min.hashCode),
        sec.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserInfoParams')
          ..add('uid', uid)
          ..add('visitCount', visitCount)
          ..add('lastVisitDateTime', lastVisitDateTime)
          ..add('screenResolution', screenResolution)
          ..add('hour', hour)
          ..add('min', min)
          ..add('sec', sec))
        .toString();
  }
}

class UserInfoParamsBuilder
    implements Builder<UserInfoParams, UserInfoParamsBuilder> {
  _$UserInfoParams _$v;

  String _uid;
  String get uid => _$this._uid;
  set uid(String uid) => _$this._uid = uid;

  int _visitCount;
  int get visitCount => _$this._visitCount;
  set visitCount(int visitCount) => _$this._visitCount = visitCount;

  int _lastVisitDateTime;
  int get lastVisitDateTime => _$this._lastVisitDateTime;
  set lastVisitDateTime(int lastVisitDateTime) =>
      _$this._lastVisitDateTime = lastVisitDateTime;

  String _screenResolution;
  String get screenResolution => _$this._screenResolution;
  set screenResolution(String screenResolution) =>
      _$this._screenResolution = screenResolution;

  String _hour;
  String get hour => _$this._hour;
  set hour(String hour) => _$this._hour = hour;

  String _min;
  String get min => _$this._min;
  set min(String min) => _$this._min = min;

  String _sec;
  String get sec => _$this._sec;
  set sec(String sec) => _$this._sec = sec;

  UserInfoParamsBuilder();

  UserInfoParamsBuilder get _$this {
    if (_$v != null) {
      _uid = _$v.uid;
      _visitCount = _$v.visitCount;
      _lastVisitDateTime = _$v.lastVisitDateTime;
      _screenResolution = _$v.screenResolution;
      _hour = _$v.hour;
      _min = _$v.min;
      _sec = _$v.sec;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserInfoParams other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserInfoParams;
  }

  @override
  void update(void Function(UserInfoParamsBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UserInfoParams build() {
    final _$result = _$v ??
        new _$UserInfoParams._(
            uid: uid,
            visitCount: visitCount,
            lastVisitDateTime: lastVisitDateTime,
            screenResolution: screenResolution,
            hour: hour,
            min: min,
            sec: sec);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
