// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'action_params.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ActionParams> _$actionParamsSerializer =
    new _$ActionParamsSerializer();

class _$ActionParamsSerializer implements StructuredSerializer<ActionParams> {
  @override
  final Iterable<Type> types = const [ActionParams, _$ActionParams];
  @override
  final String wireName = 'ActionParams';

  @override
  Iterable<Object> serialize(Serializers serializers, ActionParams object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.customVariables != null) {
      result
        ..add('cvar')
        ..add(serializers.serialize(object.customVariables,
            specifiedType: const FullType(JsonObject)));
    }
    if (object.externalUrl != null) {
      result
        ..add('link')
        ..add(serializers.serialize(object.externalUrl,
            specifiedType: const FullType(String)));
    }
    if (object.downloadUrl != null) {
      result
        ..add('download')
        ..add(serializers.serialize(object.downloadUrl,
            specifiedType: const FullType(String)));
    }
    if (object.search != null) {
      result
        ..add('search')
        ..add(serializers.serialize(object.search,
            specifiedType: const FullType(String)));
    }
    if (object.searchCategory != null) {
      result
        ..add('search_cat')
        ..add(serializers.serialize(object.searchCategory,
            specifiedType: const FullType(String)));
    }
    if (object.searchCount != null) {
      result
        ..add('search_count')
        ..add(serializers.serialize(object.searchCount,
            specifiedType: const FullType(int)));
    }
    if (object.pageViewId != null) {
      result
        ..add('pv_id')
        ..add(serializers.serialize(object.pageViewId,
            specifiedType: const FullType(String)));
    }
    if (object.goalId != null) {
      result
        ..add('idglobal')
        ..add(serializers.serialize(object.goalId,
            specifiedType: const FullType(String)));
    }
    if (object.generationTime != null) {
      result
        ..add('gt_ms')
        ..add(serializers.serialize(object.generationTime,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  ActionParams deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ActionParamsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'cvar':
          result.customVariables = serializers.deserialize(value,
              specifiedType: const FullType(JsonObject)) as JsonObject;
          break;
        case 'link':
          result.externalUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'download':
          result.downloadUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'search':
          result.search = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'search_cat':
          result.searchCategory = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'search_count':
          result.searchCount = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'pv_id':
          result.pageViewId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'idglobal':
          result.goalId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'gt_ms':
          result.generationTime = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$ActionParams extends ActionParams {
  @override
  final JsonObject customVariables;
  @override
  final String externalUrl;
  @override
  final String downloadUrl;
  @override
  final String search;
  @override
  final String searchCategory;
  @override
  final int searchCount;
  @override
  final String pageViewId;
  @override
  final String goalId;
  @override
  final int generationTime;

  factory _$ActionParams([void Function(ActionParamsBuilder) updates]) =>
      (new ActionParamsBuilder()..update(updates)).build();

  _$ActionParams._(
      {this.customVariables,
      this.externalUrl,
      this.downloadUrl,
      this.search,
      this.searchCategory,
      this.searchCount,
      this.pageViewId,
      this.goalId,
      this.generationTime})
      : super._();

  @override
  ActionParams rebuild(void Function(ActionParamsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ActionParamsBuilder toBuilder() => new ActionParamsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ActionParams &&
        customVariables == other.customVariables &&
        externalUrl == other.externalUrl &&
        downloadUrl == other.downloadUrl &&
        search == other.search &&
        searchCategory == other.searchCategory &&
        searchCount == other.searchCount &&
        pageViewId == other.pageViewId &&
        goalId == other.goalId &&
        generationTime == other.generationTime;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc($jc(0, customVariables.hashCode),
                                    externalUrl.hashCode),
                                downloadUrl.hashCode),
                            search.hashCode),
                        searchCategory.hashCode),
                    searchCount.hashCode),
                pageViewId.hashCode),
            goalId.hashCode),
        generationTime.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ActionParams')
          ..add('customVariables', customVariables)
          ..add('externalUrl', externalUrl)
          ..add('downloadUrl', downloadUrl)
          ..add('search', search)
          ..add('searchCategory', searchCategory)
          ..add('searchCount', searchCount)
          ..add('pageViewId', pageViewId)
          ..add('goalId', goalId)
          ..add('generationTime', generationTime))
        .toString();
  }
}

class ActionParamsBuilder
    implements Builder<ActionParams, ActionParamsBuilder> {
  _$ActionParams _$v;

  JsonObject _customVariables;
  JsonObject get customVariables => _$this._customVariables;
  set customVariables(JsonObject customVariables) =>
      _$this._customVariables = customVariables;

  String _externalUrl;
  String get externalUrl => _$this._externalUrl;
  set externalUrl(String externalUrl) => _$this._externalUrl = externalUrl;

  String _downloadUrl;
  String get downloadUrl => _$this._downloadUrl;
  set downloadUrl(String downloadUrl) => _$this._downloadUrl = downloadUrl;

  String _search;
  String get search => _$this._search;
  set search(String search) => _$this._search = search;

  String _searchCategory;
  String get searchCategory => _$this._searchCategory;
  set searchCategory(String searchCategory) =>
      _$this._searchCategory = searchCategory;

  int _searchCount;
  int get searchCount => _$this._searchCount;
  set searchCount(int searchCount) => _$this._searchCount = searchCount;

  String _pageViewId;
  String get pageViewId => _$this._pageViewId;
  set pageViewId(String pageViewId) => _$this._pageViewId = pageViewId;

  String _goalId;
  String get goalId => _$this._goalId;
  set goalId(String goalId) => _$this._goalId = goalId;

  int _generationTime;
  int get generationTime => _$this._generationTime;
  set generationTime(int generationTime) =>
      _$this._generationTime = generationTime;

  ActionParamsBuilder();

  ActionParamsBuilder get _$this {
    if (_$v != null) {
      _customVariables = _$v.customVariables;
      _externalUrl = _$v.externalUrl;
      _downloadUrl = _$v.downloadUrl;
      _search = _$v.search;
      _searchCategory = _$v.searchCategory;
      _searchCount = _$v.searchCount;
      _pageViewId = _$v.pageViewId;
      _goalId = _$v.goalId;
      _generationTime = _$v.generationTime;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ActionParams other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ActionParams;
  }

  @override
  void update(void Function(ActionParamsBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ActionParams build() {
    final _$result = _$v ??
        new _$ActionParams._(
            customVariables: customVariables,
            externalUrl: externalUrl,
            downloadUrl: downloadUrl,
            search: search,
            searchCategory: searchCategory,
            searchCount: searchCount,
            pageViewId: pageViewId,
            goalId: goalId,
            generationTime: generationTime);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
