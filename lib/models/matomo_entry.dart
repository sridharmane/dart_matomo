import 'package:moor/moor.dart';

class MatomoEntry extends Table {
  DateTimeColumn get timestamp => dateTime()();
  TextColumn get queryParams => text()();
}
