import 'package:dart_matomo/dart_matomo.dart';
import 'package:flutter/material.dart';

class AnalyticsRouteObserver extends RouteObserver<PageRoute<dynamic>> {
  Future<void> _recordPageView(
      String pageName, String oldRouteName, String newRouteName) async {
    print('$oldRouteName -> $newRouteName');
    if (pageName != null) {
      await DartMatomo.instance
          .trackPageView(action: pageName, route: newRouteName);
    }
  }

  _getName(Route route) {
    if (route is PageRoute) {
      return route.settings.name;
    } else if (route is ModalRoute) {
      return route.navigator.context.visitChildElements((Element v) {
        print('Modal Route');
        print(v);
      });
    } else if (route is PopupRoute) {
      return route.settings.arguments;
    } else {
      return '${route?.toString() ?? ''}';
    }
  }

  @override
  void didPush(Route route, Route previousRoute) async {
    String routeName = _getName(route);
    String previousRouteName = _getName(previousRoute);
    await _recordPageView(routeName, previousRouteName, routeName);
    super.didPush(route, previousRoute);
  }
}
