library dart_matomo;

import 'dart:convert';
import 'dart:math';

import 'package:dart_matomo/models/action_params.dart';
import 'package:dart_matomo/models/content_params.dart';
import 'package:dart_matomo/models/entry.dart';
import 'package:dart_matomo/models/event_params.dart';
import 'package:dart_matomo/models/matomo_params.dart';
import 'package:http/http.dart' as http;

class DartMatomo {
  static DartMatomo get instance => _instance;
  static DartMatomo _instance = DartMatomo._internal();
  DartMatomo._internal();

  factory DartMatomo() {
    return instance;
  }

  String tableName;
  int _eventCounter = 0;
  int _eventsBatchCount;

  String baseUrl;
  String requiredParams;

  MatomoParams _params;

  int _lastVisit;
  int _visitCount;
  String _screenResolution;
  String _deviceId;

  init() async {
    baseUrl = ""; // TODO: Add URL
    await _setupBasicParams();
  }

  String _generateRandomStr({int length = 8}) {
    final random = Random.secure();
    return base64Url
        .encode(List<int>.generate(length, (i) => random.nextInt(50)));
  }

  ///
  /// Example: trackEvent(
  ///   name: 'BioListItem'
  ///   action: 'Clicked'
  ///   category: 'Bios'
  ///   target: 'bioId'
  ///   currentRoute: '/home'
  /// )
  ///
  Future<void> trackEvent({
    String name,
    String action,
    String category,
    String target,
    String currentRoute,
  }) async {
    if (_params != null) {
      MatomoParams params = _params.rebuild(
        (b) => b
          ..requiredParams = _params.requiredParams
              .rebuild(
                (b) => b
                  ..url = currentRoute
                  ..actionName = name
                  ..randStr = _generateRandomStr()
                  ..dateTime = DateTime.now().microsecondsSinceEpoch,
              )
              .toBuilder()
          ..contentParams = ContentParams(
            (b) => b..target = target,
          ).toBuilder()
          ..eventParams = EventParams(
            (b) => b
              ..name = name
              ..action = action,
          ).toBuilder(),
      );
      await add(Entry(
        (b) => b
          ..timestamp = params.requiredParams.dateTime
          ..queryParams = params.asQueryParams,
      ));
    }
  }

  trackAppLaunch({
    String name = 'AppLaunch',
    String action,
    String category,
    String target,
    String currentRoute,
  }) async {
    if (_params != null) {
      MatomoParams params = _params.rebuild(
        (b) => b
          ..requiredParams = _params.requiredParams
              .rebuild(
                (b) => b
                  ..url = '/'
                  ..actionName = name
                  ..randStr = _generateRandomStr()
                  ..dateTime = DateTime.now().microsecondsSinceEpoch,
              )
              .toBuilder()
          ..userParams = _params.userParams
              .rebuild(
                (b) => b
                  ..lastVisitDateTime = _lastVisit
                  ..visitCount = _visitCount
                  ..screenResolution = _screenResolution
                  ..uid,
              )
              .toBuilder()
          ..eventParams = EventParams(
            (b) => b
              ..name = name
              ..action = action,
          ).toBuilder(),
      );
      await add(Entry(
        (b) => b
          ..timestamp = params.requiredParams.dateTime
          ..queryParams = params.asQueryParams,
      ));
    }
  }

  trackPageView({
    String route,
    String action,
    String category = 'PageView',
    String target,
  }) async {
    if (_params != null) {
      /// Convert relative url to absolute url
      if (route.startsWith('/')) {
        route = 'https:/$route';
      }
      MatomoParams params = _params.rebuild(
        (b) => b
          ..requiredParams = _params.requiredParams
              .rebuild(
                (b) => b
                  ..url = route // prepend with http:/ for Matomo
                  ..actionName = action
                  ..randStr = _generateRandomStr()
                  ..dateTime = DateTime.now().microsecondsSinceEpoch,
              )
              .toBuilder()
          ..userParams = _params.userParams
              .rebuild(
                (b) => b
                  ..lastVisitDateTime = _lastVisit
                  ..visitCount = _visitCount
                  ..screenResolution = _screenResolution
                  ..uid,
              )
              .toBuilder()
          ..eventParams = EventParams(
            (b) => b
              ..name = category
              ..action = action,
          ).toBuilder(),
      );
      await add(Entry(
        (b) => b
          ..timestamp = params.requiredParams.dateTime
          ..queryParams = params.asQueryParams,
      ));
    }
  }

  Future<void> trackSearch({
    String pageName,
    String keyword,
    String category,
    int resultCount,
  }) async {
    if (_params != null) {
      MatomoParams params = _params.rebuild(
        (b) => b
          ..requiredParams = _params.requiredParams
              .rebuild(
                (b) => b
                  ..url = 'https:/$pageName' // prepend with http:/ for Matomo
                  ..actionName = pageName
                  ..randStr = _generateRandomStr()
                  ..dateTime = DateTime.now().microsecondsSinceEpoch,
              )
              .toBuilder()
          ..userParams = _params.userParams
              .rebuild(
                (b) => b
                  ..lastVisitDateTime = _lastVisit
                  ..visitCount = _visitCount
                  ..screenResolution = _screenResolution
                  ..uid,
              )
              .toBuilder()
          ..eventParams = EventParams(
            (b) => b
              ..name = category
              ..action = pageName,
          ).toBuilder()
          ..actionParams = ActionParams((b) => b
            ..search = keyword
            ..searchCategory = category
            ..searchCount = resultCount).toBuilder(),
      );
      await add(Entry(
        (b) => b
          ..timestamp = params.requiredParams.dateTime
          ..queryParams = params.asQueryParams,
      ));
    }
  }

  trackOutlink({
    String route,
    String action,
    String category = 'PageView',
  }) async {
    if (_params != null) {
      /// Convert relative url to absolute url
      if (route.startsWith('/')) {
        route = 'https:/$route';
      }
      MatomoParams params = _params.rebuild(
        (b) => b
          ..requiredParams = _params.requiredParams
              .rebuild(
                (b) => b
                  ..url = route // prepend with http:/ for Matomo
                  ..actionName = action
                  ..randStr = _generateRandomStr()
                  ..dateTime = DateTime.now().microsecondsSinceEpoch,
              )
              .toBuilder()
          ..userParams = _params.userParams
              .rebuild(
                (b) => b
                  ..lastVisitDateTime = _lastVisit
                  ..visitCount = _visitCount
                  ..screenResolution = _screenResolution
                  ..uid,
              )
              .toBuilder()
          ..eventParams = EventParams(
            (b) => b
              ..name = category
              ..action = action,
          ).toBuilder()
          ..actionParams = ActionParams(
            (b) => b..externalUrl = route,
          ).toBuilder(),
      );
      await add(Entry(
        (b) => b
          ..timestamp = params.requiredParams.dateTime
          ..queryParams = params.asQueryParams,
      ));
    }
  }

  @override
  Future<void> add(Entry data) async {
//    Database db = await DBService.instance.db;
//    Map map = serializers.serializeWith(Entry.serializer, data);
//    await db.insert(
//      tableName,
//      map,
//      conflictAlgorithm: ConflictAlgorithm.replace,
//    );
//    _eventCounter++;
//    log.fine('Added to DB. Current counter $_eventCounter/$_eventsBatchCount');
//    if (_eventCounter >= _eventsBatchCount) {
//      await _upload();
//      _eventCounter = 0;
//    }
  }

  @override
  Future<void> addAll(List<Entry> data) {
    // TODO: implement addAll
    return null;
  }

  @override
  Future<void> delete(String id) {
    // TODO: implement delete
    return null;
  }

  @override
  Future<Entry> get(String id) {
    // TODO: implement get
    return null;
  }

  @override
  Future<List<Entry>> getAll() async {
//    Database db = await DBService.instance.db;
//
//    ///
//    /// Note that for Matomo to store your tracking data accurately,
//    /// your tracking requests should be sent in chronological order
//    /// (the oldest requests should appear first).
//    ///
//    List<Map<String, dynamic>> result = await db.query(tableName,
//        orderBy: '${EntryFields.timestamp} DESC');
//    log.fine('getAll: fetched ${result.length} rows');
//
//    return result.map((row) {
//      return serializers.deserializeWith(Entry.serializer, row);
//    }).toList();
  }

  @override
  Future<void> update(Entry data) {
    // TODO: implement update
    return null;
  }

  Future<void> _setupBasicParams() async {
//    _lastVisit = int.tryParse(
//      await SecureStorage.instance.get(SecureStorageKeys.lastVisit),
//    );
//    _visitCount = int.tryParse(
//      await SecureStorage.instance.get(SecureStorageKeys.visitCount),
//    );
//    _screenResolution =
//        await SecureStorage.instance.get(SecureStorageKeys.screenResolution);
//    _deviceId = await SecureStorage.instance.get(SecureStorageKeys.deviceId);
//    log.fine('Using Device Id: $_deviceId');
  }

  Future<void> _upload() async {
    try {
      final result = await getAll();
      await send(entries: result.map((entry) => entry.queryParams).toList());
      await deleteAllWithTimestamps(result.map((entry) => entry.timestamp));
    } catch (e, stackTrace) {
      throw e;
    }
  }

  Future<void> deleteAllWithTimestamps(Iterable<int> timestamps) async {
//    Database db = await DBService.instance.db;
//    db.rawQuery(
//        'DELETE FROM $tableName WHERE ${EntryFields.timestamp} IN (${timestamps.join(', ')})');
  }

  Future<http.Response> send({List<String> entries}) async {
    try {
      if (entries.length == 1) {
        return await http.get(baseUrl + entries[0]);
      }
      return await http.post(
        baseUrl,
        body: Map.from({
          'requests': entries,
        }),
      );
    } catch (e, stackTrace) {
      throw e;
    }
  }
}
